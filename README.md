# tekton-first-pipeline
## Description
Cette pipeline va telecharger un repos sur gitlab est synchronisé sur ArgoCD au sein d'un cluster OpenShift. 
## Tasks
### cleanup
Nettoie le workspace qui pourrait être remplis lors d'un précédent run de la pipeline.
### git-alpine-task

Cette tache va cloner un repos. Pour permettre cette action, nous allons monter en volume 
    le secret. Ensuite nous allons copier la cle ssh en root avec les bonnes permissions. 
    nous ajoutons StrictHostKeyChecking à no afin qu'il ne pose aucunes questions et ajoute automatiquement la nouvelle cle
    d hotes dans le fichier /known_hosts.
    Un workspace est utilisé ainsi que le volumemount qui nous sert a passer le secret contenant le rsa 
    et known_host de gitlab.

### modkustomize
Cette tache va modifier le kustomize test (de la branche du même nom) de notre repo ArgoCD. Elle va simplement exécuter un script qui va modifier le nombre de replicats.
si les replicats sont à 3, elle le modifie à 4 et vice versa
### git-push-task

Cette tache va nous permettre de push sur un repos git. Pour permettre cette action, nous allons monter en volume 
    le secret. Ensuite nous allons copier la cle ssh en root avec les bonnes permissions. 
    nous ajoutons StrictHostKeyChecking à no afin qu'il ne pose aucunes questions et ajoute automatiquement la nouvelle cle
    d hotes dans le fichier /known_hosts.
    Nous définissons un mail / username en plus.
    Un workspace est utilisé ainsi que le volumemount qui nous sert a passer le secret contenant le rsa et known_host de gitlab.
